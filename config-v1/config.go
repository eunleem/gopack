package config

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"strings"

	"gopkg.in/yaml.v2"
)

func LoadFile(config interface{}, fileName string) (err error) {
	file, err := ioutil.ReadFile(fileName)
	if err != nil {
		return err
	}

	fileName = strings.ToLower(fileName)

	if strings.HasSuffix(fileName, ".yaml") {
		return yaml.Unmarshal(file, config)

	} else if strings.HasSuffix(fileName, ".json") {
		return json.Unmarshal(file, config)
	}

	log.Print("Unsupported file format.")
	return errors.New("unsupported file format")
}
