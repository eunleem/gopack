package config

import (
	"gopkg.in/mgo.v2"
)

type MongoDbConfig struct {
	Host     string   `yaml:"host" json:"host"`
	Username string   `yaml:"username" json:"username"`
	Password string   `yaml:"password" json:"password"`
	Database string   `yaml:"database,omitempty" json:"database,omitempty"`
	Mode     mgo.Mode `yaml:"mode,omitempty" json:"mode,omitempty"`
}

type MongoDb struct {
	MongoDb MongoDbConfig `yaml:"mongoDb" json:"mongoDb"`
}

func (i *MongoDb) Load(fileName string) error {
	return LoadFile(i, fileName)
}
