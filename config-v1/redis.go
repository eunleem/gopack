package config

type RedisConfig struct {
	Host     string `json:"host" yaml:"host"`
	Password string `json:"password" yaml:"password"`
	DbNumber int    `json:"dbNumber,omitempty" yaml:"dbNumber,omitempty"`
}

type Redis struct {
	Redis RedisConfig `yaml:"redis" json:"redis"`
}

func (i *Redis) Load(fileName string) error {
	return LoadFile(i, fileName)
}
