package config

type WebModuleConfig struct {
	IsDevMode bool `json:"isDevMode" yaml:"isDevMode"`
}

type WebModule struct {
	WebModule WebModuleConfig `json:"module" yaml:"module"`
	MongoDb   MongoDbConfig   `json:"mongoDb" yaml:"mongoDb"`
	Redis     RedisConfig     `json:"redis" yaml:"redis"`
}

func (i *WebModule) Load(fileName string) error {
	return LoadFile(i, fileName)
}
