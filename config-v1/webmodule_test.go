package config

import (
	"testing"
)

func TestWebModuleYaml(t *testing.T) {
	var conf WebModule
	if err := conf.Load("webmodule.test.yaml"); err != nil {
		t.Error("Error loading webmodule.test.yaml")
	}

	if conf.WebModule.IsDevMode != true {
		t.Error("Invalid isDevMode")
	}

	if conf.MongoDb.Host != "localhost" {
		t.Error("Invalid MongoDb.Host")
	}

	if conf.MongoDb.Username != "user" {
		t.Error("Invalid MongoDb.Username")
	}

	if conf.MongoDb.Password != "pass" {
		t.Error("Invalid MongoDb.Password")
	}

	if conf.MongoDb.Database != "database" {
		t.Error("Invalid MongoDb.Database")
	}

	if conf.Redis.Host != "localhost" {
		t.Error("Invalid Redis.Host")
	}

	if conf.Redis.Password != "pass" {
		t.Error("Invalid Redis.Password")
	}

	if conf.Redis.DbNumber != 0 {
		t.Error("Invalid Redis.DbNumber")
	}
}
