package config

type WebServerConfig struct {
	Name        string `json:"name,omitempty" yaml:"name,omitempty"`
	Version     string `json:"version" yaml:"version,omitempty"`
	Domain      string `json:"domain" yaml:"domain"`
	Port        int    `json:"port" yaml:"port"`
	FullAddress string `json:"fullAddress" yaml:"fullAddress,omitempty"`
	WebDir      string `json:"webDir" yaml:"webDir"`
}

type WebServer struct {
	WebServer WebServerConfig `yaml:"server" json:"server"`
}

func (i *WebServer) Load(fileName string) error {
	return LoadFile(i, fileName)
}
