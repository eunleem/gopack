package config

import (
	"testing"
)

func TestWebServerYaml(t *testing.T) {

	var conf WebServer
	if err := conf.Load("webserver.test.yaml"); err != nil {
		t.Error("Error loading webserver.test.yaml")
	}

	if conf.WebServer.Domain != "helloworld.com" {
		t.Error("Invalid WebServer.Domain")
	}

	if conf.WebServer.WebDir != "./" {
		t.Error("Invalid WebServer.WebDir")
	}

	if conf.WebServer.Port != 8080 {
		t.Error("Invalid WebServer.Port")
	}

	if conf.WebServer.Version != "1.0" {
		t.Error("Invalid WebServer.Version")
	}

}
