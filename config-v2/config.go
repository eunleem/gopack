package config

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"strings"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/yaml.v2"
)

func Load(config interface{}, filePath string) (err error) {
	file, err := ioutil.ReadFile(filePath)
	if err != nil {
		return err
	}

	if strings.HasSuffix(filePath, ".yaml") {
		return yaml.Unmarshal(file, config)

	} else if strings.HasSuffix(filePath, ".json") {
		return json.Unmarshal(file, config)
	}

	log.Print("Unsupported file format.")
	return errors.New("unsupported file format")
}

type Config interface {
	Load(path string) error
}

type Server struct {
	Name        string `json:"name,omitempty" yaml:"name,omitempty"`
	Version     string `json:"version,omitempty" yaml:"version,omitempty"`
	Domain      string `json:"domain" yaml:"domain"`
	Port        int    `json:"port" yaml:"port"`
	FullAddress string `json:"fullAddress,omitempty" yaml:"fullAddress,omitempty"`
	WebDir      string `json:"webDir" yaml:"webDir"`
	NgDir       string `json:"ngDir" yaml:"ngDir"`
}

func (i *Server) Load(filePath string) error {
	return Load(i, filePath)
}

type Redis struct {
	Host     string `json:"host" yaml:"host"`
	Password string `json:"password" yaml:"password"`
	DbNumber int    `json:"dbNumber,omitempty" yaml:"dbNumber,omitempty"`
}

func (i *Redis) Load(filePath string) error {
	return Load(i, filePath)
}

type MongoDb struct {
	Host     string   `yaml:"host" json:"host"`
	Username string   `yaml:"username" json:"username"`
	Password string   `yaml:"password" json:"password"`
	Database string   `yaml:"database,omitempty" json:"database,omitempty"`
	Mode     mgo.Mode `yaml:"mode,omitempty" json:"mode,omitempty"`
}

func (i *MongoDb) Load(filePath string) error {
	return Load(i, filePath)
}
