package elasticemail

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strconv"
)

type BatchStatus struct {
	ShowAbuse        bool
	ShowClicked      bool
	ShowDelivered    bool
	ShowErrors       bool
	ShowFailed       bool
	ShowMessageIDs   bool
	ShowOpened       bool
	ShowPending      bool
	ShowSent         bool
	ShowUnsubscribed bool
}

func (i BatchStatus) ToQueryString() (query string) {
	if i.ShowAbuse {
		query += "&showAbuse=" + strconv.FormatBool(i.ShowAbuse)
	}
	if i.ShowClicked {
		query += "&showClicked=" + strconv.FormatBool(i.ShowClicked)
	}
	if i.ShowDelivered {
		query += "&showDelivered=" + strconv.FormatBool(i.ShowDelivered)
	}
	if i.ShowErrors {
		query += "&showErrors=" + strconv.FormatBool(i.ShowErrors)
	}
	if i.ShowFailed {
		query += "&showFailed=" + strconv.FormatBool(i.ShowFailed)
	}
	if i.ShowMessageIDs {
		query += "&showMessageIDs=" + strconv.FormatBool(i.ShowMessageIDs)
	}
	if i.ShowOpened {
		query += "&showOpened=" + strconv.FormatBool(i.ShowOpened)
	}
	if i.ShowPending {
		query += "&showPending=" + strconv.FormatBool(i.ShowPending)
	}
	if i.ShowSent {
		query += "&showSent=" + strconv.FormatBool(i.ShowSent)
	}
	if i.ShowUnsubscribed {
		query += "&showUnsubscribed=" + strconv.FormatBool(i.ShowUnsubscribed)
	}
	return query
}

type EmailJobStatus struct {
	// AbuseReports []string
	AbuseReportsCount int `json:"abuseReportsCount"`
	// Clicked  []string
	ClickedCount      int      `json:"clickedCount"`
	FailedCount       int      `json:"failedCount"`
	MessageIDs        []string `json:"messageIDs"`
	OpenedCount       int      `json:"openedCount"`
	PendingCount      int      `json:"pendingCount"`
	RecipientsCount   int      `json:"recipientsCount`
	SentCount         int      `json:"sentCount"`
	Status            string   `json:"status"`
	UnsubscribedCount int      `json:"unsubscribedCount"`
}

type BatchStatusResponse struct {
	Success bool           `json:"success"`
	Error   string         `json:"error"`
	Data    EmailJobStatus `json:"data"`
}

func GetBatchStatus(transactionId string, batchStatus BatchStatus) (response BatchStatusResponse, err error) {
	urlPath := "https://api.elasticemail.com/v2/email/getstatus"

	query := "?apikey=" + conf.ApiKey +
		"&transactionID=" + url.QueryEscape(transactionId) +
		batchStatus.ToQueryString()

	resp, err := http.Post(urlPath+query, "", nil)
	if err != nil {
		log.Print(err)
		return response, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Print(err)
		return response, err
	}

	if err := json.Unmarshal(body, &response); err != nil {
		log.Print(err)
		return response, err
	}

	return response, nil
}
