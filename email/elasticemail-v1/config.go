package elasticemail

import (
	"log"

	config "gitlab.com/eunleem/gopack/config-v1"
)

var conf = emailConfig{
	ApiKey:    "",
	FromName:  "Life In O",
	FromEmail: "mailer@lifeino.com",
}

type emailConfig struct {
	ApiKey    string `yaml:"apiKey" json:"apiKey"`
	FromName  string `yaml:"fromName" json:"fromName"`
	FromEmail string `yaml:"fromEmail" json:"fromEmail"`
}

func LoadConfig(filePath string) {
	log.Printf("Elastic Email config file: \"%s\"\n", filePath)

	if err := config.LoadFile(&conf, filePath); err != nil {
		panic(err)
	}

	// TODO Validate Config Values
}
