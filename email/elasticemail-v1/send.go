package elasticemail

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strconv"
)

/*
	# Elastic Email API
	REF: http://api.elasticemail.com/public/help#Email_Send

	## Parameters
	* msgTo: List of email recipients. Separated by comma or semicolon. (Each email is treated seperately)
	* to: Obsolete. Don't use unless compatibility with APIv1 is required.
*/

type EmailSend struct {
	MessageId     string `json:"messageID"`
	TransactionId string `json:"transactionID"`
}

type EmailSendResponse struct {
	Success bool      `json:"success"`
	Data    EmailSend `json:"data"`
	Error   string    `json:"error"`
}

func Send(to string, subject string, bodyHtml string, isTransactional bool) (response EmailSendResponse, err error) {
	if conf.ApiKey == "" {
		panic("Config file must be loaded first")
	}

	urlPath := "https://api.elasticemail.com/v2/email/send"

	// bodyText := "Body Text"
	// query := "?apikey=" + url.QueryEscape(conf.ApiKey) +
	// 	"&from=" + url.QueryEscape(conf.FromEmail) +
	// 	"&fromName=" + url.QueryEscape(conf.FromName) +
	// 	"&msgTo=" + url.QueryEscape(to) +
	// 	"&subject=" + url.QueryEscape(subject) +
	// 	"&bodyHtml=" + url.QueryEscape(bodyHtml) +
	// 	"&charset=" + url.QueryEscape("utf-8") +
	// 	// "&bodyText=" + url.QueryEscape(bodyText) +
	// 	"&isTransactional=" + strconv.FormatBool(isTransactional)

	// log.Printf("Sending %d long request", len(urlPath+query))

	resp, err := http.PostForm(urlPath,
		url.Values{
			"apikey":          {conf.ApiKey},
			"from":            {conf.FromEmail},
			"msgTo":           {to},
			"subject":         {subject},
			"bodyHtml":        {bodyHtml},
			"isTransactional": {strconv.FormatBool(isTransactional)},
		})

	// resp, err := http.Post(urlPath+query, "", nil)
	if err != nil {
		log.Print(err)
		return response, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Print(err)
		return response, err
	}
	log.Print(string(body))

	if err := json.Unmarshal(body, &response); err != nil {
		log.Print(err)
		return response, err
	}

	return response, nil
}
