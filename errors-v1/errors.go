package err

const (
	TypeUndefined = iota
	TypeDatabase  = iota
	TypeFormat    = iota
	TypeAccount   = iota
)

type ServerError struct {
	Type   int
	Title  string
	Detail string
}

// func New(t int, msg string) ServerError {
// 	return ServerError{t, msg}
// }

func (i *ServerError) SetDetail(s string) *ServerError {
	i.Detail = s
	return i
}

func (i *ServerError) SetErr(err error) *ServerError {
	if err == nil {
		return nil
	}
	i.Detail = err.Error()
	return i
}

func (i *ServerError) Error() string {
	return i.Title
}

var (
	Undefined = &ServerError{
		Type:  TypeUndefined,
		Title: "undefined error",
	}
)

var (
	Database = &ServerError{
		Type:  TypeDatabase,
		Title: "general database error",
	}

	DbInsert = &ServerError{
		Type:  TypeDatabase,
		Title: "insert error",
	}

	DbUpdate = &ServerError{
		Type:  TypeDatabase,
		Title: "update error",
	}

	DbDelete = &ServerError{
		Type:  TypeDatabase,
		Title: "delete error",
	}

	DataNotFound = &ServerError{
		Type:  TypeDatabase,
		Title: "delete error",
	}

	DataDuplicate = &ServerError{
		Type:  TypeDatabase,
		Title: "duplicate",
	}
)

var (
	InvalidFormat = &ServerError{
		Type:  TypeFormat,
		Title: "invalid format",
	}

	MissingRequired = &ServerError{
		Type:  TypeFormat,
		Title: "missing required value",
	}

	InvalidRange = &ServerError{
		Type:  TypeFormat,
		Title: "invalid range",
	}

	InvalidEmail = &ServerError{
		Type:  TypeFormat,
		Title: "invalid email",
	}

	ErrInvalidUsername = &ServerError{
		Type:  TypeFormat,
		Title: "invalid username",
	}

	InvalidPassword = &ServerError{
		Type:  TypeFormat,
		Title: "invalid username",
	}

	TooLong = &ServerError{
		Type:  TypeFormat,
		Title: "too long",
	}

	Binding = &ServerError{
		Type:  TypeFormat,
		Title: "binding error",
	}
)

var (
	NotLoggedIn = &ServerError{
		Type:  TypeAccount,
		Title: "not logged in",
	}

	NotAuthorized = &ServerError{
		Type:  TypeAccount,
		Title: "not authorized",
	}

	AuthenticationFailed = &ServerError{
		Type:  TypeAccount,
		Title: "authentication failed",
	}
)
