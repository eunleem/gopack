package lioerrors

import (
	"fmt"
)

type Type int

const (
	TypeUndefined Type = iota
	TypeDatabase  Type = iota
	TypeInput     Type = iota
	TypeAccount   Type = iota
)

type Server struct {
	errType  Type
	detail   string
	jrespMsg string
}

func New(t Type, msg string) *Server {
	return &Server{t, msg, ""}
}

func (i *Server) GetDetail() string {
	return i.detail
}

func (i *Server) GetType() Type {
	return i.errType
}

func (i *Server) GetTypeStr() string {
	switch i.errType {
	case TypeUndefined:
		return "undefined"
	case TypeAccount:
		return "account"
	case TypeDatabase:
		return "database"
	case TypeInput:
		return "input"
	default:
		return "undefined"
	}
}

func (i *Server) GetJrespMsg() string {
	return i.jrespMsg
}

func (i *Server) SetDetail(s string) *Server {
	i.detail = s
	return i
}

func (i *Server) SetErr(err error) *Server {
	if err == nil {
		return nil
	}
	i.detail = err.Error()
	return i
}

func (i *Server) SetClientMsg(msg string) {
	i.jrespMsg = msg
}

func (i *Server) Error() string {
	return fmt.Sprintf("ServerError: %s", i.detail)
}

var (
	Undefined = &Server{
		errType:  TypeUndefined,
		detail:   "undefined error",
		jrespMsg: "Undefined error has occurred!",
	}
)

// Database related errors
var (
	Database = &Server{
		errType:  TypeDatabase,
		detail:   "general database error",
		jrespMsg: "General database error has occurred!",
	}

	DbInsert = &Server{
		errType:  TypeDatabase,
		detail:   "insert error",
		jrespMsg: "Failed to insert to database.",
	}

	DbUpdate = &Server{
		errType:  TypeDatabase,
		detail:   "update error",
		jrespMsg: "Faled to update database.",
	}

	DbDelete = &Server{
		errType:  TypeDatabase,
		detail:   "delete error",
		jrespMsg: "Failed to delete from database.",
	}

	DataNotFound = &Server{
		errType:  TypeDatabase,
		detail:   "data not found",
		jrespMsg: "Data not found",
	}

	DataDuplicate = &Server{
		errType:  TypeDatabase,
		detail:   "duplicate",
		jrespMsg: "Duplicate data",
	}
)

var (
	InvalidFormat = &Server{
		errType: TypeInput,
		detail:  "invalid format",
	}

	Required = &Server{
		errType: TypeInput,
		detail:  "missing required value",
	}

	Binding = &Server{
		errType: TypeInput,
		detail:  "binding error",
	}
)

// Account related Errors
var (
	// Unauthenticated means login is required but not logged in
	Unauthenticated = &Server{
		errType: TypeAccount,
		detail:  "Unauthenticated",
	}

	// Unauthorized means logged in but does not have sufficient rights
	Unauthorized = &Server{
		errType: TypeAccount,
		detail:  "unauthorized",
	}

	AuthenticationFailed = &Server{
		errType: TypeAccount,
		detail:  "authentication failed",
	}

	// Account suspended
	Banned = &Server{
		errType: TypeAccount,
		detail:  "banned",
	}
)
