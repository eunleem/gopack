package gingular

import (
	"log"
	"strings"

	"github.com/gin-gonic/gin"
)

var dirPath = "../web/dist/"
var baseHref = ""

func init() {
	dirPath = formatDirPath(dirPath)

	baseHref = strings.Trim(baseHref, "/")
}

func formatDirPath(directoryPath string) string {
	if strings.HasSuffix(directoryPath, "/") == false {
		directoryPath += "/"
	}
	return directoryPath
}

func set(directoryPath string, baseHrefLocal string) {
	dirPath = formatDirPath(directoryPath)
	baseHref = strings.Trim(baseHrefLocal, "/")
}

func Dev(dirPathLocal string, baseHrefLocal string) gin.HandlerFunc {
	set(dirPathLocal, baseHrefLocal)
	return func(c *gin.Context) {
		path := c.Request.URL.EscapedPath()[1:]
		arr := strings.Split(path, "/")

		if strings.HasSuffix(path, ".bundle.js.map") {
			fileName := arr[len(arr)-1]
			c.File(dirPath + fileName)
			c.Next()
			return
		}

		c.Next()
	}
}

func Common(dirPathLocal string, baseHrefLocal string) gin.HandlerFunc {
	set(dirPathLocal, baseHrefLocal)
	return func(c *gin.Context) {
		path := c.Request.URL.EscapedPath()[1:]
		arr := strings.Split(path, "/")

		if strings.HasSuffix(path, ".bundle.js") {
			fileName := arr[len(arr)-1]
			c.File(dirPath + fileName)
			c.Next()
			return
		}

		if strings.HasSuffix(path, ".chunk.js") {
			fileName := arr[len(arr)-1]
			c.File(dirPath + fileName)
			c.Next()
			return
		}

		if strings.HasSuffix(path, ".bundle.css") {
			fileName := arr[len(arr)-1]
			c.File(dirPath + fileName)
			c.Next()
			return
		}

		if strings.Compare(arr[0], baseHref) == 0 ||
			strings.Compare(arr[0]+"/", baseHref) == 0 {
			log.Print(dirPath + "index.html")
			c.File(dirPath + "index.html")
			c.Next()
			return
		}

		c.Next()
	}
}
