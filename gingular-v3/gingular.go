// For Angular 6
package gingular

import (
	"log"
	"net/http"
	"strings"

	"github.com/gin-gonic/contrib/sessions"

	"github.com/gin-gonic/gin"
)

var dirPath = "../web/dist/"
var baseHref = ""

func init() {
	dirPath = formatDirPath(dirPath)

	baseHref = strings.Trim(baseHref, "/")
}

func formatDirPath(directoryPath string) string {
	if strings.HasSuffix(directoryPath, "/") == false {
		directoryPath += "/"
	}
	return directoryPath
}

func set(directoryPath string, baseHrefLocal string) {
	dirPath = formatDirPath(directoryPath)
	baseHref = strings.Trim(baseHrefLocal, "/")
}

func Dev(dirPathLocal string, baseHrefLocal string) gin.HandlerFunc {
	set(dirPathLocal, baseHrefLocal)
	return func(c *gin.Context) {
		path := c.Request.URL.EscapedPath()[1:]
		arr := strings.Split(path, "/")

		if strings.Compare(arr[0], baseHref) == 0 ||
			strings.Compare(arr[0]+"/", baseHref) == 0 {

			if strings.HasSuffix(path, ".js.map") {
				fileName := arr[len(arr)-1]
				c.File(dirPath + fileName)
				c.Next()
				return
			}

			// log.Print(dirPath + "index.html")
			// c.File(dirPath + "index.html")
			// c.Next()
			return
		}

		c.Next()
	}
}

func Common(dirPathLocal string, baseHrefLocal string, requireLogin bool) gin.HandlerFunc {
	set(dirPathLocal, baseHrefLocal)
	return func(c *gin.Context) {
		path := c.Request.URL.EscapedPath()[1:]
		arr := strings.Split(path, "/")

		if strings.Compare(arr[0], baseHref) == 0 ||
			strings.Compare(arr[0]+"/", baseHref) == 0 {

			if requireLogin == true {
				s := sessions.Default(c)
				id := s.Get("accountid")
				if id == nil {
					c.Redirect(http.StatusTemporaryRedirect, "/login")
					c.Next()
					return
				}
			}

			log.Print(path)

			if strings.HasSuffix(path, ".js") {
				log.Print("serving .js")
				fileName := arr[len(arr)-1]
				c.File(dirPath + fileName)
				c.Next()
				return

			} else if strings.HasSuffix(path, ".css") {
				log.Print("serving .css")
				fileName := arr[len(arr)-1]
				c.File(dirPath + fileName)
				c.Next()
				return

			} else if strings.HasSuffix(path, ".js.map") {
				log.Print("serving .map")
				fileName := arr[len(arr)-1]
				c.File(dirPath + fileName)
				c.Next()
				return

			}

			log.Print(dirPath + "index.html")
			c.File(dirPath + "index.html")
			c.Next()
			return
		}

		c.Next()
		// p := c.Request.URL.EscapedPath()
		// path := p[1:]
		// arr := strings.Split(path, "/")

		// if strings.Compare(arr[0], baseHref) == 0 ||
		// 	strings.Compare(arr[0]+"/", baseHref) == 0 {

		// 	if requireLogin == true {
		// 		s := sessions.Default(c)
		// 		id := s.Get("accountid")
		// 		if id == nil {
		// 			c.Redirect(http.StatusTemporaryRedirect, "/login")
		// 			c.Next()
		// 			return
		// 		}
		// 	}

		// 	log.Print(dirPath + "index.html")
		// 	c.File(dirPath + "index.html")
		// 	c.Next()
		// 	return
		// }

		// if p == "/main.js" ||
		// 	p == "/runtime.js" ||
		// 	p == "/vendor.js" ||
		// 	p == "/styles.js" ||
		// 	p == "/polyfills.js" {
		// 	fileName := arr[len(arr)-1]
		// 	c.File(dirPath + fileName)
		// 	c.Next()
		// 	return
		// }

		// c.Next()
	}
}
