package modconf

import (
	"gitlab.com/eunleem/gopack/ginmodcore-v1/api"
	"gitlab.com/eunleem/gopack/ginmodcore-v1/modevent"
	"gopkg.in/mgo.v2"
)

//type Module interface {
//	Attach(*mgo.Database, *api.ApiList, *eventmanager.EventManager)
//}

type Config struct {
	Name        string
	Db          *mgo.Database
	Collection  *mgo.Collection
	Api         *api.List
	EventEngine *modevent.Engine
	IsDevMode   bool
}

// func (i *Config) Set(name string, db *mgo.Database, collection *mgo.Collection,
// 	api *api.List, eventEngine *modevent.Engine, isDevMode bool) *Config {
//
// 	i.Name = name
// 	i.Db = db
// 	i.Collection = collection
// 	i.Api = api
// 	i.EventEngine = eventEngine
// 	i.IsDevMode = isDevMode
//
// 	return i
// }
//
// func (i *Config) Run() {
//
// }
