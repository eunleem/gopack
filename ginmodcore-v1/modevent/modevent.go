package modevent

type Engine struct {
	handlersMap map[int]*EventHandlerQueue
}

func (i *Engine) On(eventId int) *EventHandlerQueue {
	if i.handlersMap[eventId] == nil {
		i.handlersMap[eventId] = &EventHandlerQueue{}
	}
	return i.handlersMap[eventId]
}

type EventHandler func(interface{})

// EventHandler2 is not recommened. Try to use EventHandler instead.
type EventHandler2 func(interface{}, interface{})

type EventHandlerQueue struct {
	handlers  []EventHandler
	handlers2 []EventHandler2
}

func (i *EventHandlerQueue) Do(fn ...EventHandler) {
	i.handlers = append(i.handlers, fn...)
}

func (i *EventHandlerQueue) Do2(fn ...EventHandler2) {
	i.handlers2 = append(i.handlers2, fn...)
}

func (i *Engine) Emit(eventID int, data interface{}) {
	if i.handlersMap[eventID] == nil {
		return
	}
	for _, fn := range i.handlersMap[eventID].handlers {
		fn(data)
	}
}

// Emit2 is Not Recommended. Try to use Emit() and pass all the data in one parameter.
func (i *Engine) Emit2(eventID int, d1 interface{}, d2 interface{}) {
	if i.handlersMap[eventID] == nil {
		return
	}
	for _, fn := range i.handlersMap[eventID].handlers2 {
		fn(d1, d2)
	}
}

// Create generates new EventManager Engine
func Create() Engine {
	return Engine{
		handlersMap: make(map[int]*EventHandlerQueue),
	}
}
