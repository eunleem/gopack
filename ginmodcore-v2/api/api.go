package api

import (
	"github.com/gin-gonic/gin"
	"log"
)

type api struct {
	Method    int
	Path      string
	HandlerFn gin.HandlerFunc
}

const (
	GET     = 1
	POST    = 2
	PUT     = 3
	DELETE  = 4
	PATCH   = 5
	HEAD    = 6
	OPTIONS = 7
)

type List struct {
	List []api
}

func (i *List) Add(method int, path string, fn gin.HandlerFunc) {
	i.List = append(i.List,
		api{method, path, fn},
	)
}

func (i *List) Register(e *gin.Engine) {
	log.Printf("%d added.", len(i.List))
	for _, val := range i.List {
		if val.Method == GET {
			log.Print("Adding " + val.Path)
			e.GET(val.Path, val.HandlerFn)

		} else if val.Method == POST {
			e.POST(val.Path, val.HandlerFn)

		} else if val.Method == PUT {
			e.PUT(val.Path, val.HandlerFn)

		} else if val.Method == DELETE {
			e.DELETE(val.Path, val.HandlerFn)

		} else if val.Method == PATCH {
			e.PATCH(val.Path, val.HandlerFn)

		} else if val.Method == HEAD {
			e.HEAD(val.Path, val.HandlerFn)

		} else if val.Method == OPTIONS {
			e.OPTIONS(val.Path, val.HandlerFn)

		}
	}
}

// type ApiList struct {
// 	List []api
// }
//
// func (i *ApiList) Add(method int, path string, fn gin.HandlerFunc) {
// 	i.List = append(i.List,
// 		api{method, path, fn},
// 	)
// }
//
// func (i *ApiList) Run(e *gin.Engine) {
// 	log.Printf("%d added.", len(i.List))
// 	for _, val := range i.List {
// 		if val.Method == GET {
// 			log.Print("Adding " + val.Path)
// 			e.GET(val.Path, val.HandlerFn)
//
// 		} else if val.Method == POST {
// 			e.POST(val.Path, val.HandlerFn)
//
// 		} else if val.Method == PUT {
// 			e.PUT(val.Path, val.HandlerFn)
//
// 		} else if val.Method == DELETE {
// 			e.DELETE(val.Path, val.HandlerFn)
//
// 		} else if val.Method == PATCH {
// 			e.PATCH(val.Path, val.HandlerFn)
//
// 		} else if val.Method == HEAD {
// 			e.HEAD(val.Path, val.HandlerFn)
//
// 		} else if val.Method == OPTIONS {
// 			e.OPTIONS(val.Path, val.HandlerFn)
//
// 		}
// 	}
// }
