package modconf

import (
	"github.com/go-redis/redis"
	"gitlab.com/eunleem/gopack/ginmodcore-v2/api"
	"gitlab.com/eunleem/gopack/ginmodcore-v2/modevent"
	"gopkg.in/mgo.v2"
)

type mongoDb struct {
	Session    *mgo.Session
	Db         *mgo.Database
	Collection *mgo.Collection
}

type redisClient struct {
	Client *redis.Client
}

type SharedData struct {
	MongoDb     mongoDb
	Redis       redisClient
	Api         *api.List
	EventEngine *modevent.Engine
	Domain      string
	FullAddress string
}

type ModuleData struct {
	Name       string
	TableName  string
	IsDevMode  bool
	Collection *mgo.Collection
	SharedData
}

func (m *ModuleData) SetSharedData(data SharedData) {
	m.Api = data.Api
	m.EventEngine = data.EventEngine
	m.MongoDb = data.MongoDb
	m.Redis = data.Redis
	m.Domain = data.Domain
	m.FullAddress = data.FullAddress
}
