package jresp

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	myerrs "gitlab.com/eunleem/gopack/errors-v2"
)

const (
	codeAlreadyLoggedIn = 100

	codeLoginFailed   = -110
	codeUserSuspended = -111 // Deprecated
	codeUserDeleted   = -112 // Deprecated

	codeAccountSuspended   = -111
	codeAccountDeleted     = -112
	codeAccountNotVerified = -113
	codeAccountLocked      = -114

	codeLoginRequired = -120

	codeNotAuthorized = -130
	codeUnauthorized  = -130

	codeBadInput                  = -200
	codeErrorBinding              = -210
	codeValueRequired             = -220
	codeInvalidValue              = -230
	codeDuplicateValue            = -240
	codeDuplicateValueEmail       = -241
	codeDuplicateValueDisplayName = -242
	codeTooMany                   = -250
	codeOverLimit                 = -260

	codeInvalidFileType = -270
	codeInvalidFileSize = -271

	codeDataNotFound  = -300
	codeDataDeleted   = -310
	codeDataSuspended = -320

	codeDocumentNotFound  = -300
	codeDocumentDeleted   = -310
	codeDocumentSuspended = -320

	codeDatabaseError = -400

	codeServerError           = -500
	codeThirdPartyServerError = -510

	codeNotImplemented    = -610
	codeNotYetImplemented = -610
)

func Auto(err *myerrs.Server, ctx *gin.Context) {
	switch err {
	case myerrs.Unauthenticated:
		ctx.JSON(http.StatusUnauthorized, gin.H{
			"status":  codeLoginRequired,
			"message": err.GetJrespMsg(),
		})
	case myerrs.Unauthorized:
		ctx.JSON(http.StatusUnauthorized, gin.H{
			"status":  codeNotAuthorized,
			"message": err.GetJrespMsg(),
		})
	case myerrs.InvalidFormat:
		ctx.JSON(http.StatusOK, gin.H{
			"status":  codeInvalidValue,
			"message": err.GetJrespMsg(),
		})
	case myerrs.DataDuplicate:
		ctx.JSON(http.StatusOK, gin.H{
			"status":  codeDuplicateValue,
			"message": err.GetJrespMsg(),
		})
	default:
		log.Printf("Type: %s\nDetail: %s\n", err.GetTypeStr(), err.GetDetail())
		ctx.JSON(http.StatusOK, gin.H{
			"status":  codeServerError,
			"message": "General error.",
		})
	}
}

func AlreadyLoggedIn(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeAlreadyLoggedIn,
		"message": "Already Logged In.",
	})
}

func LoginRequired(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeLoginRequired,
		"message": "Login Required.",
	})
}

func NotAuthorized(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeNotAuthorized,
		"message": "Not Authorized.",
	})
}

func LoginFailed(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeLoginFailed,
		"message": "Login Failed.",
	})
}

func AccountDeleted(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeAccountDeleted,
		"message": "Account Deleted.",
	})
}

// UserDeleted is deprecated. Use AccountDeleted instead
func UserDeleted(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeUserDeleted,
		"message": "User Deleted.",
	})
}

func AccountNotVerified(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeAccountNotVerified,
		"message": "Account Not Verified.",
	})
}

func AccountLocked(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeAccountLocked,
		"message": "Account is under temporary lockdown.",
	})
}

func AccountSuspended(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeAccountSuspended,
		"message": "Account Suspended.",
	})
}

// UserSuspended is deprecated. Use AccountSuspended instead.
func UserSuspended(ctx *gin.Context) {
	log.Print("UserSuspend deprecated. USE AccountSuspended instead")
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeUserSuspended,
		"message": "User Suspended.",
	})
}

func TooMany(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeTooMany,
		"message": "Too Many.",
	})
}

func OverLimit(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeOverLimit,
		"message": "Over Limit.",
	})
}

func InvalidFileType(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeInvalidFileType,
		"message": "Over Limit.",
	})
}

func BadInput(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeBadInput,
		"message": "Provided values are unacceptable or missing.",
	})
}

func ErrorBinding(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeErrorBinding,
		"message": "Error binding data.",
	})
}

func ValueRequired(ctx *gin.Context, values ...string) {
	message := ""
	for _, val := range values {
		message += " " + val
	}
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeValueRequired,
		"message": "Missing Required Value!" + message,
	})
}

func InvalidValue(ctx *gin.Context, message string) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeInvalidValue,
		"message": message,
	})
}

func DuplicateValue(ctx *gin.Context, message string) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeDuplicateValue,
		"message": "Duplicate Value! " + message,
	})
}

func DuplicateValueEmail(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":     codeDuplicateValueEmail,
		"message":    "Duplicate Email! ",
		"messageKor": "중복된 이메일 주소 입니다.",
	})
}

func DuplicateValueDisplayName(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":     codeDuplicateValue,
		"message":    "Duplicate Display Name!",
		"messageKor": "중복된 닉네임 입니다.",
	})
}

func DataNotFound(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeDataNotFound,
		"message": "Data Not Found.",
	})
}

func DocumentNotFound(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeDocumentNotFound,
		"message": "Data Not Found.",
	})
}

func DocumentDeleted(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeDocumentDeleted,
		"message": "Data Deleted.",
	})
}

func DocumentSuspended(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeDocumentSuspended,
		"message": "Data Not Found.",
	})
}

func DatabaseError(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeDatabaseError,
		"message": "Database Error.",
	})
}

func ServerError(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeServerError,
		"message": "Server Error.",
	})
}

func ThirdPartyServerError(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeThirdPartyServerError,
		"message": "ThirdParty Server Error.",
	})
}

func NotImplemented(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeNotYetImplemented,
		"message": "Not Yet Implemented.",
	})
}

// NotYetImplemented is deprecated. Use NotImplemented instead.
func NotYetImplemented(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeNotYetImplemented,
		"message": "Not Yet Implemented.",
	})
}

func Successful(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  0,
		"message": "Successful.",
	})
}
