package jresp

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

const (
	codeAlreadyLoggedIn       = 100
	codeLoginFailed           = -110
	codeUserSuspended         = -111
	codeUserDeleted           = -112
	codeAccountNotVerified    = -113
	codeLoginRequired         = -120
	codeNotAuthorized         = -130
	codeBadInput              = -200
	codeErrorBinding          = -210
	codeValueRequired         = -220
	codeInvalidValue          = -230
	codeDuplicateValue        = -240
	codeTooMany               = -250
	codeOverLimit             = -260
	codeDataNotFound          = -300
	codeDocumentNotFound      = -300
	codeDocumentDeleted       = -310
	codeDocumentSuspended     = -320
	codeDatabaseError         = -400
	codeServerError           = -500
	codeThirdPartyServerError = -510
	codeNotImplemented        = -610
	codeNotYetImplemented     = -610
)

func AlreadyLoggedIn(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeAlreadyLoggedIn,
		"message": "Already Logged In.",
	})
}

func LoginRequired(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeLoginRequired,
		"message": "Login Required.",
	})
}

func NotAuthorized(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeNotAuthorized,
		"message": "Not Authorized.",
	})
}

func LoginFailed(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeLoginFailed,
		"message": "Login Failed.",
	})
}

func UserDeleted(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeUserDeleted,
		"message": "User Deleted.",
	})
}

func AccountNotVerified(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeAccountNotVerified,
		"message": "Account Not Verified.",
	})
}

func UserSuspended(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeUserSuspended,
		"message": "User Suspended.",
	})
}

func TooMany(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeTooMany,
		"message": "Too Many.",
	})
}

func OverLimit(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeOverLimit,
		"message": "Over Limit.",
	})
}

func BadInput(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeBadInput,
		"message": "Provided values are unacceptable or missing.",
	})
}

func ErrorBinding(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeErrorBinding,
		"message": "Error binding data.",
	})
}

func ValueRequired(ctx *gin.Context, values ...string) {
	message := ""
	for _, val := range values {
		message += " " + val
	}
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeValueRequired,
		"message": "Missing Required Value!" + message,
	})
}

func InvalidValue(ctx *gin.Context, message string) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeInvalidValue,
		"message": message,
	})
}

func DuplicateValue(ctx *gin.Context, message string) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeDuplicateValue,
		"message": "Duplicate Value! " + message,
	})
}

func DataNotFound(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeDataNotFound,
		"message": "Data Not Found.",
	})
}

func DocumentNotFound(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeDocumentNotFound,
		"message": "Data Not Found.",
	})
}

func DocumentDeleted(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeDocumentDeleted,
		"message": "Data Deleted.",
	})
}

func DocumentSuspended(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeDocumentSuspended,
		"message": "Data Not Found.",
	})
}

func DatabaseError(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeDatabaseError,
		"message": "Database Error.",
	})
}

func ServerError(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeServerError,
		"message": "Server Error.",
	})
}

func ThirdPartyServerError(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeThirdPartyServerError,
		"message": "ThirdParty Server Error.",
	})
}

func NotImplemented(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeNotYetImplemented,
		"message": "Not Yet Implemented.",
	})
}

// NotYetImplemented is deprecated. Use NotImplemented instead.
func NotYetImplemented(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  codeNotYetImplemented,
		"message": "Not Yet Implemented.",
	})
}

func Successful(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"status":  0,
		"message": "Successful.",
	})
}
