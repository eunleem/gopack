#!/bin/bash

printf "Module directory name: "
read dirName
mkdir -p ./vendor/modules/$dirName

moduleName=$dirName

printf "Module package name: "
read packageName

printf "Module table name: "
read tableName

cat <<EOF > ./vendor/modules/$dirName/api.go
package $packageName

import (
	"log"
	"net/http"
	// "strconv"
	// "strings"
	// "time"

	engine "github.com/gin-gonic/gin"

	"gitlab.com/eunleem/gopack/ginmodcore-v2/api"
	"gitlab.com/eunleem/gopack/ginutil-v1/jresp"
	"gitlab.com/eunleem/gopack/mgoutil-v1"

	// "modevent"
  modaccount "modules/account"
)


func setApis() {
	const apiVersion = "/api/v1"
	const rootPath = "/$moduleName"
	const apiPath = apiVersion + rootPath

	m.Api.Add(api.GET, apiPath+"/example", example)
	m.Api.Add(api.POST, apiPath+"/example", example)
}

func example(ctx *engine.Context) {
	isLoggedIn, myAccount := modaccount.CheckLoggedIn(ctx)
	if isLoggedIn == false {
		jresp.LoginRequired(ctx)
		return
	}
	// if modaccount.IsAdmin(myAccount) == false {
	//	 jresp.NotAuthorized(ctx)
	// 	 return
	// }

	// m.EventEngine.Emit(modevent.UserInsert, &newAccount)

	ctx.JSON(http.StatusOK, engine.H{
		"status":  0,
		"message": "Request successful.",
	})
}

EOF

cat <<EOF > ./vendor/modules/$dirName/bll.go
package $packageName
// BLL file contains functions that are public
EOF

cat <<EOF > ./vendor/modules/$dirName/data.go
package $packageName

import (
	"log"
	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func setIndexes() {
	index := mgo.Index{
		Key:        []string{"username", "email"},
		Unique:     true,
		DropDups:   false,
		Background: true,
		Sparse:     true,
	}

	if err := m.MongoDb.Collection.EnsureIndex(index); err != nil {
		log.Printf("Error while ensuring index for '%s'. %s", m.Name, err.Error())
		panic(err)
	}
}

type TYPE struct {
	Id             bson.ObjectId \`bson:"_id" json:"id"\`
	AccountId      bson.ObjectId \`json:"accountId"\`
	AccountId      bson.ObjectId \`json:"accountId"\`
	Email          string        \`bson:"email" json:"email,omitempty" form:"email"\`
	Username       string        \`bson:",omitempty" json:"username,omitempty" form:"username"\`
	IsDeleted      bool      		 \`json:"-"\`
	UpdateTime     time.Time 		 \`json:"updateTime"\`
	CreateTime     time.Time 		 \`json:"createTime"\`
}

func (i *TYPE) GetById(id bson.ObjectId) error {
	if err := m.Collection.FindId(id).One(&i); err != nil {
		log.Print(err)
		log.Println("Could not find TYPE by Id.")
		return err
	}
	return nil
}

func (i *TYPE) Insert() error {
	if i.Id.Valid() == false {
		i.Id = bson.NewObjectId()
	}

	if i.CreateTime.IsZero() {
		i.CreateTime = time.Now()
	}

	if err := m.Collection.Insert(&i); err != nil {
		log.Print(err)
		log.Println("Could not insert an TYPE.")
		return err
	}

	log.Println("Inserted an TYPE.")

	return nil
}

func (i *TYPE) Update() (err error) {
	if err = m.Collection.UpdateId(i.Id, &i); err != nil {
		log.Print(err)
		log.Println("Could not update an TYPE.")
	}

	return err
}

func (i *TYPE) Delete() (err error) {
	if err = m.Collection.RemoveId(i.Id); err != nil {
		log.Print(err)
		log.Println("Could not delete a TYPE.")
	}

	return err
}




EOF

cat <<EOF > ./vendor/modules/$dirName/event.go
package $packageName
// event.go file defines related events

import (
	//"log"
	"modevent"
)

func setEvents() {
	m.EventEngine.On(modevent.AccountInsert).Do(eventWelcome)
}

func eventWelcome(d interface{}) {
	//i := d.(*Account)
	//log.Print(i.Id.Hex() + " has joined. Welcome!")
}

EOF

cat <<EOF > ./vendor/modules/$dirName/helper.go
package $packageName
// helper.go defines private functions
EOF


cat <<EOF > ./vendor/modules/$dirName/module.go
package $packageName

import (
  "log"
  "strings"

	"gitlab.com/eunleem/gopack/ginmodcore-v2/modconf"
)

const (
	moduleName = "$moduleName"
	tableName  = "$tableName"
)

var m modconf.ModuleData

func Init(data modconf.SharedData, devMode bool) {
	m.SetSharedData(data)

	m.IsDevMode = devMode
	m.Name = moduleName
	m.TableName = tableName
	m.Collection = m.MongoDb.Db.C(tableName)
	m.MongoDb.Collection = m.MongoDb.Db.C(tableName)

	// TODO Load module config if set to use it

	setIndexes()
	setApis()
	setEvents()

	log.Print(strings.Title(moduleName) + " module loaded")
}
EOF
