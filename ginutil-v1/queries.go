package ginutil

import (
	"errors"
	"strconv"

	"github.com/globalsign/mgo/bson"

	"github.com/gin-gonic/gin"
)

// Id gets ID from Param("id")
func Id(ctx *gin.Context) (id bson.ObjectId, err error) {
	idStr := ctx.Param("id")
	if bson.IsObjectIdHex(idStr) == false {
		return id, errors.New("invalid objectid")
	}

	return bson.ObjectIdHex(idStr), nil
}

func ParamObjectId(ctx *gin.Context, name string) (id bson.ObjectId, err error) {
	idStr := ctx.Param(name)
	if bson.IsObjectIdHex(idStr) == false {
		return id, errors.New("invalid objectid")
	}
	return bson.ObjectIdHex(idStr), nil
}

// BasicQueries return sort, limit, skip from ctx *gin.Context
func BasicQueries(ctx *gin.Context) (sort string, limit, skip int) {
	return Sort(ctx), Limit(ctx), Skip(ctx)
}

func Sort(ctx *gin.Context) (sort string) {
	return ctx.DefaultQuery("sort", "-createtime")
}

func Limit(ctx *gin.Context) int {
	limitStr := ctx.DefaultQuery("limit", "20")
	limit, err := strconv.Atoi(limitStr)
	if err != nil {
		limit = 20
	}

	return limit

}

func Skip(ctx *gin.Context) int {
	skipStr := ctx.DefaultQuery("skip", "0")
	skip, err := strconv.Atoi(skipStr)
	if err != nil {
		skip = 0
	}

	return skip
}
