package jconfig

import (
	"encoding/json"
	"gopkg.in/mgo.v2"
	"io/ioutil"
)

type MongoDb struct {
	Host     string   `json:"host"`
	Username string   `json:"username"`
	Password string   `json:"password"`
	Database string   `json:"database"`
	Mode     mgo.Mode `json:"mode"`
}

type Redis struct {
	Host     string `json:"host"`
	Password string `json:"password"`
	DbNumber int    `json:"dbNumber"`
}

type WebServer struct {
	Domain  string  `json:"domain"`
	WebDir  string  `json:"webDir"`
	Port    int     `json:"port"`
	Version string  `json:"version"`
	MongoDb MongoDb `json:"mongoDb"`
	Redis   Redis   `json:"redis"`
}

// ServerConfig is deprecated use WebServer instead
type ServerConfig struct {
	Name            string `json:"name"`
	Version         string `json:"version"`
	Port            int    `json:"port"`
	DbName          string `json:"dbName"`
	WebDir          string `json:"webDir"`
	Domain          string `json:"domain"`
	RedisHost       string `json:"redisHost"`
	RedisPass       string `json:"redisPass"`
	MongoDbHost     string `json:"mongoDbHost"`
	MongoDbUsername string `json:"mongoDbUsername"`
	MongoDbPass     string `json:"mongoDbPass"`
}

func (i *WebServer) Load(fileName string) (err error) {
	file, err := ioutil.ReadFile(fileName)
	if err != nil {
		return err
	}

	err = json.Unmarshal(file, &i)
	if err != nil {
		return err
	}

	return err
}

func (i *ServerConfig) Load(fileName string) (err error) {
	file, err := ioutil.ReadFile(fileName)
	if err != nil {
		return err
	}

	err = json.Unmarshal(file, &i)
	if err != nil {
		return err
	}

	return err
}

func LoadJsonFile(config interface{}, fileName string) (err error) {
	file, err := ioutil.ReadFile(fileName)
	if err != nil {
		return err
	}

	err = json.Unmarshal(file, &config)
	if err != nil {
		return err
	}

	return err
}
