package mgoutil

import (
	"errors"
	"log"
	"time"

	"gitlab.com/eunleem/gopack/config-v1"

	// "gopkg.in/mgo.v2"
	// "gopkg.in/mgo.v2/bson"
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

func OpenSessionFromFile(fileName string) (session *mgo.Session, err error) {
	var conf config.MongoDb
	if err = config.LoadFile(conf, fileName); err != nil {
		return session, err
	}

	mgoConf := conf.MongoDb
	return OpenSession(mgoConf.Host, mgoConf.Username, mgoConf.Password)
}

func OpenDbFromFile(fileName string) (db *mgo.Database, err error) {
	var conf config.MongoDb
	if err = config.LoadFile(conf, fileName); err != nil {
		return db, err
	}

	mgoConf := conf.MongoDb
	dbSession, err := OpenSession(mgoConf.Host, mgoConf.Username, mgoConf.Password)
	if err != nil {
		return db, err
	}
	return dbSession.DB(mgoConf.Database), nil
}

func OpenSession(dbHost string, username string, password string) (dbSession *mgo.Session, err error) {
	if username == "" {
		dbSession, err = mgo.Dial(dbHost)
		if err != nil {
			log.Println("Could not connect to MongoDB!")
			panic(err)
		}

	} else {
		info := mgo.DialInfo{
			Addrs:    []string{dbHost},
			Timeout:  10 * time.Second,
			Username: username,
			Password: password,
		}

		dbSession, err = mgo.DialWithInfo(&info)
		if err != nil {
			log.Println("Could not connect to MongoDB!")
			panic(err)
		}
	}

	dbSession.SetMode(mgo.Monotonic, true)
	return dbSession, nil
}

// CloseSession closes DB connection
func CloseSession(dbSession *mgo.Session) {
	dbSession.Close()
}

// IsUnique checks whether value in give field is unique.
func IsUnique(dbc *mgo.Collection, field string, value interface{}) bool {
	if count, err := dbc.Find(bson.M{field: value}).Count(); err != nil {
		log.Print(err)
		return false

	} else if count > 0 {
		return false
	}

	return true
}

// StrToObjectID converts Str to bson.ObjectId. Invalid str returns err != nil
func StrToObjectID(idStr string) (bson.ObjectId, error) {
	if bson.IsObjectIdHex(idStr) == false {
		return "", errors.New("Invalid ObjectId")
	}
	return bson.ObjectIdHex(idStr), nil
}
