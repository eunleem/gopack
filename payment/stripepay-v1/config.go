package stripepay

import (
	"log"
	"strings"

	stripe "github.com/stripe/stripe-go"

	"gitlab.com/eunleem/gopack/config-v1"
)

type stripeConfig struct {
	SecretKey      string `yaml:"secretKey" binding:"required"`
	PublishableKey string `yaml:"publishableKey" binding:"required"`
	// Currency string `yaml:"currency,omitempty"`
}

var conf stripeConfig

// LoadConfig must be called before using this module
// fileName is usually "stripe.dev.yaml"
func LoadConfig(fileName string) {
	lowerCaseFileName := strings.ToLower(fileName)
	if strings.HasSuffix(lowerCaseFileName, ".yaml") == false &&
		strings.HasSuffix(lowerCaseFileName, ".yml") == false {

		log.Print("package stripepay invalid config file")
		panic("only .yaml or .yml file is suppored")
	}

	if err := config.LoadFile(&conf, fileName); err != nil {
		log.Print("package stripepay could not load config file")
		panic(err)
	}

	stripe.Key = conf.SecretKey
}
