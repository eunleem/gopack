package stripepay

import (
	"errors"
	"log"
	"strings"

	valid "github.com/asaskevich/govalidator"
	stripe "github.com/stripe/stripe-go"
	stripeCharge "github.com/stripe/stripe-go/charge"
	stripeCustomer "github.com/stripe/stripe-go/customer"
)

// CreateCustomer creates customer on stripe using card token
// token is created using Stripe.js or Checkout!
func CreateCustomer(token string, email string, name string, accountId string, organizationId string) (*stripe.Customer, error) {
	if valid.IsEmail(email) == false {
		return nil, errors.New("invalid email")
	}
	email = strings.ToLower(email)

	// Create a Customer:
	customerParams := &stripe.CustomerParams{
		Email: email,
		Desc:  name,
	}

	customerParams.AddMeta("accountid", accountId)
	customerParams.AddMeta("organizationid", organizationId)
	customerParams.AddMeta("name", name)

	customerParams.SetSource(token)

	customer, err := stripeCustomer.New(customerParams)
	if err != nil {
		return nil, err
	}

	return customer, nil
}

// UpdateCustomer updates customer card info using token
func UpdateCustomer(customerId string, token string) (*stripe.Customer, error) {
	src := &stripe.SourceParams{
		Token: token,
	}
	return stripeCustomer.Update(
		customerId,
		&stripe.CustomerParams{
			Source: src,
		},
	)
}

func DeleteCustomer(customerId string) (*stripe.Customer, error) {
	params := &stripe.CustomerParams{}
	return stripeCustomer.Del(customerId, params)
}

// ChargeCustomer charges customer from existing Customer Info
func ChargeCustomer(customerId string, amount uint64, description string) (*stripe.Charge, error) {
	chargeParams := &stripe.ChargeParams{
		Amount:   amount, // 1000 is 10.00 Dollars
		Currency: "usd",
		Customer: customerId,
		Desc:     description,
		//Statement: "", // < 22 chars, no <, >, ', or ".
	}

	// chargeParams.AddMeta("order_id", "6735") // Metadata

	result, err := stripeCharge.New(chargeParams)
	if err != nil {
		log.Print(err)
		return nil, err
	}

	return result, nil
}
