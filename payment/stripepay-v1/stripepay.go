// Package stripepay is a wrapper around stripe-go library
package stripepay

import (
	"errors"
	"log"
	"strings"

	stripe "github.com/stripe/stripe-go"
	stripeCharge "github.com/stripe/stripe-go/charge"
	stripeRefund "github.com/stripe/stripe-go/refund"

	"strconv"
)

/*
	# Stripe Payment

	Docs: https://stripe.com/docs/charges

  `token` is created using Stripe.js or Checkout!
  test token: "tok_visa"
  amount is uint64. 1000 = $10.00

  meta: Max 20 keys, key names up to 40 chars long, values up to 500 chars long
*/

func init() {
}

// GetPublishableKey returns PublishableKey for Stripe.
// This key is used... idk...
func GetPublishableKey() string {
	return conf.PublishableKey
}

// ChargeCard charges card from token
// https://stripe.com/docs/api#charges See create charge section
func ChargeCard(token string, amount uint64, description string, statement string) (*stripe.Charge, error) {
	if len(statement) > 22 {
		return nil, errors.New("statement too long")
	}

	if strings.ContainsAny(statement, `<>'"`) {
		return nil, errors.New("statement illegal char")
	}

	chargeParams := &stripe.ChargeParams{
		Amount:    amount, // 1000 is 10.00 Dollars
		Currency:  "usd",
		Desc:      description,
		Statement: statement, // < 22 chars, no <, >, ', or ".
	}

	// chargeParams.AddMeta("order_id", "6735") // Metadata

	chargeParams.SetSource(token)

	result, err := stripeCharge.New(chargeParams)
	if err != nil {
		log.Print(err)
		return nil, err
	}

	return result, nil
}

// GetCharge gets charge info by charge id
// https://stripe.com/docs/api#charges See retrieve a charge section
func GetCharge(chargeId string) (*stripe.Charge, error) {
	charge, err := stripeCharge.Get(chargeId, nil)
	if err != nil {
		log.Print(err)
		return nil, err
	}

	return charge, err
}

// GetAllChargesByCustomerId gets all charges info by customer id
// https://stripe.com/docs/api#charges See retrieve a charge section
func GetAllChargesByCustomerId(customerId string, limit uint64) []*stripe.Charge {
	limitStr := strconv.FormatUint(limit, 10)

	params := &stripe.ChargeListParams{}

	params.Filters.AddFilter("limit", "", limitStr)
	params.Filters.AddFilter("customer", "", customerId)

	i := stripeCharge.List(params)

	charges := make([]*stripe.Charge, limit)
	for i.Next() {
		c := i.Charge()
		charges = append(charges, c)
	}

	return charges
}

// Refund refunds the fund from the charge
// When Amount == 0, it issues a full refund.
// Docs: https://stripe.com/docs/api#create_refund
func Refund(chargeId string, amount uint64) (*stripe.Refund, error) {
	params := &stripe.RefundParams{
		Charge: chargeId,
	}

	if amount > 0 {
		params.Amount = amount
	}

	return stripeRefund.New(params)
}
