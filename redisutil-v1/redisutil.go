package redisutil

import (
	"errors"
	"log"

	sessions "github.com/gin-gonic/contrib/sessions"
	"github.com/go-redis/redis"
)

// New gets Redis Client for general Redis use
func New(host string, password string, dbNumber int) (*redis.Client, error) {
	if host == "" {
		host = "localhost:6379"
	}
	client := redis.NewClient(&redis.Options{
		Addr:     host,
		Password: password,
		DB:       dbNumber, // 0 is default
	})
	if err := client.Ping().Err(); err != nil {
		log.Print("Failed to create new Redis Client")
		log.Print(err)
		return client, errors.New("redis client")
	}

	return client, nil
}

// NewSession gets Redis Session for gin-gonic framework
func NewSession(addr string, password string) (store sessions.RedisStore, err error) {
	// randomStr := randstr.AlphaNum(12)
	store, err = sessions.NewRedisStore(10, "tcp",
		addr,
		password,
		[]byte("RanDox(*Str)d*!&"),
	)
	if err != nil {
		log.Print("Could not connect to Redis.")
		return store, err
	}

	return store, err
}
