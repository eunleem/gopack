package emailaddr

import (
	"errors"
	"strings"

	"regexp"

	valid "github.com/asaskevich/govalidator"
)

var alphanumRegexp *regexp.Regexp

func init() {
	alphanumRegexp = regexp.MustCompile(`^[A-Za-z0-9][A-Za-z0-9_-]*$`)
}

// ToUniqueEmail strips ignored chars and returns addr that is unique.
// i.e. john.smith+a@gmail.com -> johnsmith@gmail.com
// Since gmail ignores . and anything after +, john.smith+a@gmail.com == johnsmith@gmail.com
// Only gmail is supported in this version
func ToUniqueEmail(email string) (string, error) {
	if len(email) > 64 {
		return "", errors.New("exceeds 64 char length")
	}
	email = strings.ToLower(email)

	if valid.IsEmail(email) == false {
		return "", errors.New("invalid email")
	}

	username := strings.Split(email, "@")[0]

	if strings.HasSuffix(email, "@gmail.com") {
		username = strings.Split(username, "+")[0]
		username = strings.Replace(username, ".", "", -1)
		if len(username) > 30 {
			return "", errors.New("gmail username too long")
		}
		if alphanumRegexp.MatchString(username) == false {
			return "", errors.New("invalid chars")
		}
		return username + "@gmail.com", nil
	}

	if alphanumRegexp.MatchString(username) == false {
		return "", errors.New("invalid chars")
	}

	return email, nil
}
