package emailaddr

import (
	"testing"
)

func TestToUniqueEmail(t *testing.T) {
	// Check if invalid email address passes
	if _, err := ToUniqueEmail("Eun.Leem+WORK@.com"); err == nil {
		t.Error("must return err when invalid email is entered")
	}

	if _, err := ToUniqueEmail("01adfsaasdfasfjasdfkjsadkfjsadfskdjfsdakjffjfsdafdsfsadfasasdfsdafasdfdsa2345678901234567890123456789a@glive.com"); err == nil {
		t.Error("email longer than 64 chars must fail")
	}

	if _, err := ToUniqueEmail("012345678901234567890123456789a@gmail.com"); err == nil {
		t.Error("gmail only supports username up to 30 chars")
	}

	if _, err := ToUniqueEmail("012345678901234567890123456789@gmail.com"); err != nil {
		t.Error("30 chars should work")
	}

	if email, err := ToUniqueEmail("Adam.SMITH+WORK@GMail.Com"); err != nil {
		t.Error(err)
	} else if email != "adamsmith@gmail.com" {
		t.Error(email)
	}

	if _, err := ToUniqueEmail("한글.SMITH+WORK@GMail.Com"); err == nil {
		t.Error("should not contain hangul")
	}

	if _, err := ToUniqueEmail("한글.+WORK@live.Com"); err == nil {
		t.Error("should not contain hangul")
	}

	if email, err := ToUniqueEmail("adam.SMITH+레이블@GMail.Com"); err != nil {
		t.Error("gmail label allows hangul")
	} else if email != "adamsmith@gmail.com" {
		t.Error(email)
	}

	if _, err := ToUniqueEmail("adam.SMITH+레이*_-블@GMail.Com"); err != nil {
		t.Error("gmail label allows some special chars")
	}

}
