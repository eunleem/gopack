package hash

import "crypto/sha512"

// String takes string and returns hashed []byte SHA512
func String(str string) []byte {
	return StringToSHA512(str)
}

func StringToSHA512(str string) []byte {
	return BytesToSHA512([]byte(str))
}

func BytesToSHA512(data []byte) []byte {
	h := sha512.New()
	h.Write(data)
	return h.Sum(nil)
}
