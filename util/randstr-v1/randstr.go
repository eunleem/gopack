package randstr

import (
	"math/rand"
	"time"
)

const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits

	allCharSet       = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-!@#$%^&*=|?<>"
	alphaNumSet      = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	upperAlphaNumSet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	easyAlphaNumSet  = "ACDEFHJKLMNPQRTVWXY34679" // Removed chars that similar to each other
	numberSet        = "01234567890"
)

var src = rand.NewSource(time.Now().UnixNano())

func MaxSecure(length int) string {
	return randomString(length, allCharSet)
}

func AlphaNum(length int) string {
	return randomString(length, alphaNumSet)
}

func UpperAlphaNum(length int) string {
	return randomString(length, upperAlphaNumSet)
}

func EasyAlphaNum(length int) string {
	return randomString(length, easyAlphaNumSet)
}

func Number(length int) string {
	return randomString(length, numberSet)
}

// #REF: Generate Random String in Go
// http://stackoverflow.com/a/31832326
func randomString(length int, letterSet string) string {
	b := make([]byte, length)
	// A src.Int63() generates 63 random bits, enough for letterIdxMax characters!
	for i, cache, remain := length-1, src.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = src.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterSet) {
			b[i] = letterSet[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return string(b)
}
