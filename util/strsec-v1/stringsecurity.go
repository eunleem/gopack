package strsec

import (
	"log"
	"strings"
	"unicode/utf8"
)

// Check returns true if there is problem
func Check(str string, maxLen int, minLen int) (isProblematic bool) {
	if minLen > 0 && utf8.RuneCountInString(str) < minLen {
		return true
	}

	if maxLen > 0 && utf8.RuneCountInString(str) > maxLen {
		return true
	}

	return Detect(str)
}

// Check returns true if there is problem
func CheckArr(strArr []string, maxArrLen int, maxLen int, minLen int) (isProblematic bool) {
	if len(strArr) > maxArrLen {
		log.Print("Array Length too big")
		return true
	}

	for _, v := range strArr {
		if minLen > 0 && utf8.RuneCountInString(v) < minLen {
			return true
		}

		if maxLen > 0 && utf8.RuneCountInString(v) > maxLen {
			return true
		}
		if Detect(v) == true {
			return true
		}
	}

	return false
}

// Detect returns true when detected dangerous chars
func Detect(str string) bool {
	// <, >, backslash, single quote, double quotes
	// if strings.IndexAny(str, "<>\\'\"") > -1 {
	// 	log.Print("Detected dangerous chars")
	// 	return true
	// }

	return strings.ContainsAny(str, "<>\\'\"")
}

// Detect returns true when detected dangerous chars
func DetectArr(strArr []string) bool {
	for _, v := range strArr {
		if Detect(v) == true {
			return true
		}
	}

	return false
}

// Sanitize removes spaces, dangerous chars for web
func Sanitize(org string) string {
	org = strings.TrimSpace(org)
	org = replaceDangerousChars(org)

	return org
}

func removeDangerousChars(org string) string {
	org = strings.Replace(org, "<", "", 0)
	org = strings.Replace(org, ">", "", 0)
	org = strings.Replace(org, "'", "", 0)  // Single Quote
	org = strings.Replace(org, "\"", "", 0) // Double Quote
	org = strings.Replace(org, "\\", "", 0) // Backslash

	return org
}

// Could be costly function
func replaceDangerousChars(org string) string {
	// org = strings.Replace(org, "&", "&amp;", 0)
	org = strings.Replace(org, "<", "&lt;", 0)
	org = strings.Replace(org, ">", "&gt;", 0)
	org = strings.Replace(org, "'", "&apos;", 0)  // Single Quote
	org = strings.Replace(org, "\"", "&quot;", 0) // Double Quote
	org = strings.Replace(org, "\\", "&bsol;", 0) // Backslash

	return org
}
